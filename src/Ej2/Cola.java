package Ej2;

import java.util.LinkedList;
import java.util.Queue;

public class Cola {
    private Queue<String> cola = new LinkedList<>();
    private int QUEUE_SIZE;
    private int MAX_WRITTEN_MESSAGES;
    private int messages;

    //creamos el constructor y los getters y setters
    public Cola(int QUEUE_SIZE, int MAX_WRITTEN_MESSAGES) {
        this.QUEUE_SIZE = QUEUE_SIZE;
        this.MAX_WRITTEN_MESSAGES = MAX_WRITTEN_MESSAGES;
    }

    public int getMAX_WRITTEN_MESSAGES() {
        return MAX_WRITTEN_MESSAGES;
    }
    public void setMAX_WRITTEN_MESSAGES(int MAX_WRITTEN_MESSAGES) {
        this.MAX_WRITTEN_MESSAGES = MAX_WRITTEN_MESSAGES;
    }

    public int getMessages() {
        return messages;
    }
    public void setMessages(int messages) {
        this.messages = messages;
    }

    //un metodo boolean que controla si la cola está vacia o no
    public synchronized boolean colaVacia() {
        if(getMessages() >= getMAX_WRITTEN_MESSAGES() && cola.size() == 0){
            return true;
        }else {
            return false;
        }
    }

    public synchronized void put(String txt){
        while(cola.size() == QUEUE_SIZE){
            try{
                wait();
            }catch(InterruptedException e){
                e.printStackTrace();
            }
        }
        if(messages < MAX_WRITTEN_MESSAGES){
            cola.add(txt);
            messages++;
        }
        notifyAll();
    }

    public synchronized String get() {
        while (cola.size() == 0) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        String rtrn = cola.poll();
        return rtrn;
    }
}