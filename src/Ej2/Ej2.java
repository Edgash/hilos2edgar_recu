package Ej2;

public class Ej2 {

    public static void main(String[] args) {
        //creo todas las variables
        int QUEUE_SIZE = 5;
        int MAX_WRITTEN_MESSAGES = 5;
        int NUM_READERS = 1;
        int NUM_WRITERS = 5;
        Readers[] readers = new Readers[NUM_WRITERS];
        Writers[] writers = new Writers[NUM_READERS];

        Cola cola = new Cola(QUEUE_SIZE, MAX_WRITTEN_MESSAGES);

        //creo los readers y los writers
        for (int i = 0 ; i < readers.length; i++){
            readers[i] = new Readers("Reader: " + (i + 1), cola);
        }
        for (int i = 0; i < writers.length; i++){
            writers[i] = new Writers("Writer: " + (i+1), cola);
        }

        //inicio los hilos de los readers y los writers
        for (int i = 0; i < writers.length; i++){
            writers[i].start();
        }
        for (int i = 0 ; i < readers.length; i++){
            readers[i].start();
        }

        //hago los joins de los readers y los writers para mantener la ejecucion de los hilos
        try{
            for (int i = 0; i < writers.length; i++){
                writers[i].join();
            }
            for (int i = 0 ; i < readers.length; i++){
                readers[i].join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
