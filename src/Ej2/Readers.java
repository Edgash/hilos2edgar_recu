package Ej2;

public class Readers extends Thread{

    private Cola cola;
    private int leidos;

    //creamos el conmstructor
    public Readers(String string, Cola cola) {
        super(string);
        this.cola = cola;
    }

    //implementamos el run del thread que ejecutara el metodo get de la clase Cola
    //siempre que no esté vacia
    @Override
    public void run() {

        while(!cola.colaVacia()){
            System.out.println(cola.get());
            leidos = leidos + leidos;
        }
        System.out.println(Thread.currentThread().getName());
    }
}

