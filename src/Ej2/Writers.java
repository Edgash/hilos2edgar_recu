package Ej2;

public class Writers extends Thread{
    Cola cola;
    int cont;

    //creamos el conmstructor
    public Writers(String string, Cola cola) {
        super(string);
        this.cola = cola;
        cont = 1;
    }

    //creamos un metodo que nos cree un numeero random
    private int randomNum(){
        int randomNum = (int) (Math.random() * 50);
        return randomNum;
    }

    @Override
    public void run(){
        while(cola.getMessages() < cola.getMAX_WRITTEN_MESSAGES()){
            try{
                Thread.sleep(randomNum());
            }catch(InterruptedException e){
                e.printStackTrace();
            }
            if (cola.getMAX_WRITTEN_MESSAGES() > cola.getMessages()){
                cola.put(Thread.currentThread().getName() + " ");
                cont++;
            }
        }
    }
}
